<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('product', ProductController::class)->middleware('auth:api');

Route::controller(ProductController::class)->group(function(){
    Route::post('product/create', [ProductController::class, 'store']);
    Route::post('product/update', [ProductController::class, 'update']);
})->middleware('auth:api');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();

});
